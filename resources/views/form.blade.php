<!DOCTYPE html>
<html>
    <head>
        <title>Form Pendaftaran</title>
    </head>
    <body>
        <form action="/welcome" method="post">
            @csrf
            <h1>Buat Account Baru</h1>
            <h2>Sign Up Form</h2>
            <label>First Name :</label>
            <br>
            <input type="text" name="firstname">
            <br>
            <br>
            <label>Last Name :</label>
            <br>
            <input type="text" name="lastname">
            <br>
            <br>
            <label>Gender</label>
            <br>
            <input type="radio" name="gender">Male
            <br>
            <input type="radio" name="gender">Female
            <br>
            <br>
            <label>Nationality</label>
            <br>
            <select name="nationality">
                <option value="id">Indonesian</option>
                <option value="sg">Singapore</option>
                <option value="my">Malaysian</option>
            </select>
            <br>
            <br>
            <label>Languange Spoken</label>
            <br>
            <input type="checkbox" name="bahasa">Bahasa Indonesia
            <br>
            <input type="checkbox" name="bahasa">English
            <br>
            <input type="checkbox" name="bahasa">Others
            <br>
            <br>
            <label>Bio</label>
            <br>
            <br>
            <textarea name="bio" rows="10" cols="30"></textarea>
            <br>
            <input type="submit" value="Sign Up">
        </form>
    </body>
</html>